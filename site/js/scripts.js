let usuario = JSON.parse(localStorage.getItem('usuario'));
let formularioLocal = JSON.parse(localStorage.getItem('formulario'));
let token = localStorage.getItem('token');
let anuncios = [];

if(usuario){
  mudarPagina(1);
  //nav.children.item(0).children.item(0).onclick = logout;
}

document.querySelector('.login button').onclick = login;

function mudarPagina(numero){
  
  let paginas = document.querySelectorAll('.pagina');
  
  for(let pagina of paginas){
    pagina.classList.remove('ativa');
  }
  paginas[numero].classList.add('ativa');
  
  if(numero === 0){
    let nav = document.querySelector('.navbar ul');
    nav.children.item(0).innerHTML = '<a class="nav-link" href="javascript:mudarPagina(0)" >Login</a>';
  }
  else{
    let nav = document.querySelector('.navbar ul');
    nav.children.item(0).innerHTML = '<a class="nav-link" href="javascript:mudarPagina(0)" >Sair</a>';  
  }
}

function login(){
  let dados = {
    funcional: document.querySelector('[name=funcional]').value,
    senha: document.querySelector('[name=senha]').value
  };
  
  fetch('http://localhost:8080/funcionario/login', {
  body: JSON.stringify(dados),
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
}).then(resposta => {
  if(resposta.status !== 200){
    return null;
  }
  token = resposta.headers.get('Authorization');
  localStorage.setItem('token', token);
  return resposta.json();
  
}).then(dados => {
  usuario = dados;
  localStorage.setItem('usuario', JSON.stringify(usuario));
  
  mudarPagina(1);
});
}

function listForm(){
  fetch('http://localhost:8080/cadastro/formulario', {
  method: 'GET',
  headers: {
    'Authorization': token
  }
}).then(resposta => {
  return resposta.json();
}).then(dados => {
  anuncios = dados;
  
  let lista = document.querySelector('.anuncios ul');
  
  for(let anuncio of anuncios){
    lista.innerHTML += `<li>${anuncio.titulo}</li>`;
  }
});
}

function listTemplates(){
  let baseurl = "http://localhost:8080/formulario/template/lista/segmento/";
  let segmento = document.querySelector("#lstSegmentos").value;
  let url = baseurl + segmento;  
  fetch(url, {
    method: 'GET',
    headers: {
      'Authorization': token
    }
  }).then(resposta => {
    return resposta.json();
  }).then(dados => {
    lstTamplate = dados;
    console.log(lstTamplate);
    
    let lista = document.querySelector('.listaTemplate');
    lista.innerHTML = '';
    
    
    for(let Tamplate of lstTamplate){
      let linha = '';
      
      linha += `<td>${Tamplate.segmento}</td>`;
      linha += `<td>${Tamplate.ramo}</td>`;
      linha += `<td>${Tamplate.dataCriacao}</td>`;
      linha += `<td>${Tamplate.dataInicioVigencia}</td>`;
      linha += `<td>${Tamplate.funcionalRespCriacao}</td></tr>`;
      
      lista.innerHTML += `<tr>${linha}</tr>`;
    }
    mudarPagina(5);
  });
}

function enviarTemplate(){
  
  $(document.getElementById('fb-editor')).getData('json');
  
  
  let dados = {
    ramo: "ramodeatividade",
    segmento: "EMP 1",
    motivo: "Exemplo",
    funcionalRespCriacao: usuario.funcional,
    dataInicioVigencia: "2018-04-18 10:00:00",
    template: jsonForm
  };
  
  
  fetch('http://localhost:8080/cadastro/formulario', {
  body: JSON.stringify(dados),
  method: 'POST',
  headers: {
    'Authorization': token,
    'Content-Type': 'application/json'
  }
}).then(resposta => {
  if(resposta.status !== 200){
    return null;
  }
  token = resposta.headers.get('Authorization');
  localStorage.setItem('token', token);
  return resposta.json();
  
}).then(dados => {
  usuario = dados;
  localStorage.setItem('usuario', JSON.stringify(usuario));
  
  mudarPagina(1);
});
}

function logout(){
  localStorage.removeItem('usuario');
}


function enviarForm(){
  
  let jsonForm = obterDadosForm();
  let jsonFormString = JSON.stringify(jsonForm);
  formularioLocal = JSON.parse(localStorage.getItem('formulario'));
  
  let dados = {
    cnpj: "300370608",
    templateCadastrado: jsonFormString
  };
  
  //console.log(JSON.stringify(dados));
  //console.log(dados);
  
  fetch('http://localhost:8080/formulario/cliente/cadastrar', {
  body: JSON.stringify(dados),
  method: 'POST',
  headers: {
    'Authorization': token,
    'Content-Type': 'application/json'
  }
}).then(resposta => {
  if(resposta.status !== 200){
    return null;
  }
  return resposta.json();
  
}).then(dados => {
  usuario = dados;
});
}

function buscaForm(){

  let baseurl = "http://localhost:8080/cliente/formulario/";
  let cnpj = document.querySelector("#codCNPJ").value;
  let url = baseurl + cnpj;  
  fetch(url, {
  method: 'GET',
  headers: {
    'Authorization': token
  }
}).then(resposta => {
  return resposta.json();
}).then(dados => {
  formulario = dados;
  /*       let  formData = JSON.parse(formulario[0].templateCadastrado);
  console.log(formData);
  var formRenderOpts = {
    i18n: {locale: 'pt-BR'},
    formData,
    dataType: 'json'
  }; */

  localStorage.setItem('formulario', JSON.stringify(formulario));
  
  jQuery(function($) {
    var fbRender = document.getElementById('fb-render'),
    a = formulario.template;

    
    let  formData = JSON.parse(a)
    var formRenderOpts = {
      i18n: {locale: 'pt-BR'},
      formData,
      dataType: 'json'
    };
    $(fbRender).formRender(formRenderOpts);
    // $('.rendered-form input').attr('readonly', 'readonly');
    //$('.rendered-form *[id*=select]').attr('readonly', 'readonly');
  });
  
  mudarPagina(4);
  //console.log(anuncios);
  //let xa = anuncios.templateCadastrado.replace(/\\"/g, '"');
  //console.log(xa);
  //console.log(JSON.parse(anuncios.templateCadastrado));
  //let lista = document.querySelector('.anuncios ul');
  
  //for(let anuncio of anuncios){
  //  lista.innerHTML += `<li>${anuncio.titulo}</li>`;
  //}
});
}

function consultaForm(){

  let baseurl = "http://localhost:8080/formulario/cliente/consultar/cnpj/";
  let cnpj = "300370608" ;
  let url = baseurl + cnpj;  
  fetch(url, {
  method: 'GET',
  headers: {
    'Authorization': token
  }
}).then(resposta => {
  return resposta.json();
}).then(dados => {
  formulario = dados;
  /*       let  formData = JSON.parse(formulario[0].templateCadastrado);
  console.log(formData);
  var formRenderOpts = {
    i18n: {locale: 'pt-BR'},
    formData,
    dataType: 'json'
  }; */

  localStorage.setItem('formulario', JSON.stringify(formulario));
  
  jQuery(function($) {
    var fbRender = document.getElementById('fb-render2'),
    a = formulario.template;

    
    let  formData = JSON.parse(a)
    var formRenderOpts = {
      i18n: {locale: 'pt-BR'},
      formData,
      dataType: 'json'
    };
    $(fbRender).formRender(formRenderOpts);
    // $('.rendered-form input').attr('readonly', 'readonly');
    //$('.rendered-form *[id*=select]').attr('readonly', 'readonly');
  });
  
  mudarPagina(6);
  //console.log(anuncios);
  //let xa = anuncios.templateCadastrado.replace(/\\"/g, '"');
  //console.log(xa);
  //console.log(JSON.parse(anuncios.templateCadastrado));
  //let lista = document.querySelector('.anuncios ul');
  
  //for(let anuncio of anuncios){
  //  lista.innerHTML += `<li>${anuncio.titulo}</li>`;
  //}
});
}

function obterDadosForm(){
  let divs = document.querySelectorAll('.rendered-form div');
  
  let json = [];
  
  for(let div of divs){
    let campo =[];
    for(i = 0; i<div.childNodes.length; i++){
      let nodeNome = div.childNodes[i].nodeName;
      if(div.childNodes.length<2){
        if(nodeNome=="H1" || nodeNome=="P")
        {
          campo = {
            type: (nodeNome=="P" ? "paragraph" : "header"),
            label: div.childNodes[i].innerHTML,
            subtype: div.childNodes[i].nodeName,
          }
          json.push(campo);
        }
      }
      else
      {
        let tipo = "";
        let valuesObj = [];
        let obj = [];
        let nome = "";
        if(div.childNodes[1].type=="select-one"){
          tipo = "select";
          let opcoes = div.childNodes[1].childNodes;
          
          for(let opcao of opcoes){
            if(opcao.selected){
              valuesObj = {
                label: opcao.innerHTML,
                value: opcao.value,
                selected: opcao.selected
              }
              obj.push(valuesObj);
            }
            else{
              valuesObj = {
                label: opcao.innerHTML,
                value: opcao.value
              }
              obj.push(valuesObj);
            }
          }
          campo = {
            type: tipo,
            label: div.childNodes[0].innerHTML,
            className: div.childNodes[1].className,
            name: div.childNodes[1].name,
            values: obj
          }
          json.push(campo);
        }
        let clsNome = "" + div.childNodes[0].className;
        if(clsNome.includes("checkbox-group", 0)){
          tipo = "checkbox-group";
          nome = div.childNodes[0].getAttribute("for");
          campo = {
            type: tipo,
            label: div.childNodes[0].innerHTML,
            className: div.childNodes[1].className,
            name: nome,
            values: obj
          }
          let opcoes = div.childNodes[1].childNodes;
          for(let opcao of opcoes){
            console.log(opcao.nodeName);
            if(opcao.nodeName=="#text"){ break}
            if(opcao.childNodes[0].checked){
              valuesObj = {
                label: opcao.childNodes[1].innerHTML,
                value: opcao.childNodes[0].value,
                selected: opcao.childNodes[0].checked
              }
              obj.push(valuesObj);
            }
            else{
              valuesObj = {
                label: opcao.childNodes[1].innerHTML,
                value: opcao.childNodes[0].value
              }
              obj.push(valuesObj);
            }
          }
          json.push(campo);
        }
      }
      
    }
    //json.push(campo);
  }
  let vetor = "[]";
  let vetores = [];
  for(vetor of json)
  {
    if(vetor.name==null && vetor.className==""){
    }
    else
    {
      vetores.push(vetor)
    }
  }
  return vetores;
}