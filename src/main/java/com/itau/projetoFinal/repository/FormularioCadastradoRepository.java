package com.itau.projetoFinal.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetoFinal.model.FormularioCadastrado;

public interface FormularioCadastradoRepository extends CrudRepository<FormularioCadastrado, Long>{

	public Optional<FormularioCadastrado> findFirstByCnpjOrderByDataCadastroDesc(String cnpj);
	public Set<FormularioCadastrado> findByCnpjOrderByDataCadastroDesc(String cnpj);

}
