package com.itau.projetoFinal.repository;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetoFinal.model.FormularioTemplate;

public interface FormularioTemplateRepository extends CrudRepository<FormularioTemplate, Long> {

	public Optional<FormularioTemplate> findFirstBySegmentoOrderByDataInicioVigenciaDesc(String segmento);
	public Optional<FormularioTemplate> findFirstByRamoOrderByDataInicioVigenciaDesc(String ramo);
	public Optional<FormularioTemplate> findFirstBySegmentoAndDataInicioVigenciaLessThanEqualOrderByDataInicioVigenciaDesc(String segmento, Date data);

	public Set<FormularioTemplate> findByRamoOrderById(String ramo);
	public Set<FormularioTemplate> findBySegmentoOrderById(String segmento);
	public Set<FormularioTemplate> findByFuncionalRespCriacaoOrderById(Long funcional);
}