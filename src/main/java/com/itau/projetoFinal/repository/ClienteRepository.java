package com.itau.projetoFinal.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetoFinal.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, String> {
	public Optional<Cliente> findByCnpj(String cnpj);

}
