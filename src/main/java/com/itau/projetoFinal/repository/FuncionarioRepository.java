package com.itau.projetoFinal.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetoFinal.model.Funcionario;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {
	public Optional<Funcionario> findByFuncional(long funcional);

}
