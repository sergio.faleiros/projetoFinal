package com.itau.projetoFinal.elemento;

public enum Segmento {

	emp1("Emp1"),
	emp2("Emp2"),
	emp3("Emp3"),
	emp4("Emp4");

	private final String segmento;

	Segmento(String segmento) {
		this.segmento = segmento;
	}

	@Override
	public String toString() {
		return this.segmento;
	}
	
	public String[] getValores() {
		String[] valores = new String[values().length];
		int i = 0;
		
		for(Segmento segmento : values()) {
			valores[i] = segmento.toString();
			i++;
		}
		
		return valores;
	}
}
