package com.itau.projetoFinal.elemento;

public enum RamoAtividade {
	
		industrial("Industrial"),
		comercial("Comercial"),
		prestacaoServicos("Prestacao de Servicos");
	
	
	private final String ramoAtividade;
	
	RamoAtividade(String ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}
	
	@Override
	public String toString() {
		return this.ramoAtividade;
	}
}
