package com.itau.projetoFinal.elemento;

public enum Cargo {
	
		Gerente("Gerente"),
		AnalistaNegocio("Analista de Negocio"),
		AnalistaSistema("Analista de Sistemas"),
		Caixa("Caixa");
	
	
	private final String cargo;
	
	Cargo(String cargo) {
		this.cargo = cargo;
	}
	
	@Override
	public String toString() {
		return this.cargo;
	}
}
