package com.itau.projetoFinal.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Funcionario {
	
	@Id
	private long funcional;
	private String nome;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;
	private String cargo;
	
	public long getFuncional() {
		return funcional;
	}
	public void setFuncional(long funcional) {
		this.funcional = funcional;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
}
