package com.itau.projetoFinal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class FormularioTemplate {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private String ramo;
	private String segmento;
	private String motivo;
	private Date dataCriacao;
	private long funcionalRespCriacao;
	private Date dataInicioVigencia;

	@Column(columnDefinition="json")
	private String template;
	
	public long getId() {	
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public long getFuncionalRespCriacao() {
		return funcionalRespCriacao;
	}

	public void setFuncionalRespCriacao(long funcionalRespCriacao) {
		this.funcionalRespCriacao = funcionalRespCriacao;
	}

	public Date getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(Date dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	@Override
	public String toString() {
		return "Formulario [id=" + id + ", ramo=" + ramo + ", segmento=" + segmento + ", motivo=" + motivo
				+ ", dataCriacao=" + dataCriacao + ", funcionalRespCriacao=" + funcionalRespCriacao
				+ ", dataInicioVigencia=" + dataInicioVigencia + ", template=" + template + "]";
	}
}
