package com.itau.projetoFinal.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class FormularioCadastrado {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private Date dataCadastro;
	private long funcionalRespCriacao;
	private String cnpj;
	
	@Column(columnDefinition="json")
	private String templateCadastrado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public long getFuncionalRespCriacao() {
		return funcionalRespCriacao;
	}

	public void setFuncionalRespCriacao(long funcionalRespCriacao) {
		this.funcionalRespCriacao = funcionalRespCriacao;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTemplateCadastrado() {
		return templateCadastrado;
	}

	public void setTemplateCadastrado(String templateCadastrado) {
		this.templateCadastrado = templateCadastrado;
	}

	@Override
	public String toString() {
		return "FormularioCadastrado [id=" + id + ", dataCadastro=" + dataCadastro + ", funcionalRespCriacao="
				+ funcionalRespCriacao + ", cnpj=" + cnpj + ", templateCadastrado=" + templateCadastrado + "]";
	}
}
