package com.itau.projetoFinal.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente {
	
	@Id
	private String cnpj;
	private String nomeRazao;
	private String segmento;
	private String ramo;
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeRazao() {
		return nomeRazao;
	}
	public void setNomeRazao(String nomeRazao) {
		this.nomeRazao = nomeRazao;
	}
	public String getSegmento() {
		return segmento;
	}
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	@Override
	public String toString() {
		return "Cliente [cnpj=" + cnpj + ", nomeRazao=" + nomeRazao + ", segmento=" + segmento + ", ramo=" + ramo + "]";
	}
}
