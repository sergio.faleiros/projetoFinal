package com.itau.projetoFinal.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.projetoFinal.elemento.Cargo;
import com.itau.projetoFinal.model.Cliente;
import com.itau.projetoFinal.model.FormularioCadastrado;
import com.itau.projetoFinal.model.FormularioTemplate;
import com.itau.projetoFinal.model.Funcionario;
import com.itau.projetoFinal.repository.ClienteRepository;
import com.itau.projetoFinal.repository.FormularioCadastradoRepository;
import com.itau.projetoFinal.repository.FormularioTemplateRepository;
import com.itau.projetoFinal.repository.FuncionarioRepository;
import com.itau.projetoFinal.service.PasswordService;
import com.itau.projetoFinal.service.TokenService;

@Controller
public class FormularioController {

	@Autowired
	PasswordService passwordService;

	@Autowired
	TokenService tokenService;

	@Autowired
	FormularioTemplateRepository formularioRepo;

	@Autowired
	FormularioCadastradoRepository formularioCadastradoRepo;

	@Autowired
	FuncionarioRepository funcionarioRepo;

	@Autowired
	ClienteRepository clienteRepo;

	@RequestMapping(path="/formulario/template/cadastrar", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> cadastrarFormulario(HttpServletRequest request, @RequestBody FormularioTemplate formularioTemplate) {	

		Optional<Funcionario> funcionarioBanco = validarFuncionarioToken(request);

		System.out.println(funcionarioBanco.get().getFuncional());
		System.out.println(funcionarioBanco.get().getCargo());

		if(! funcionarioBanco.isPresent()) {
			System.out.println("Funcionario nao logado");
			return ResponseEntity.badRequest().build();
		}

		if (! funcionarioBanco.get().getCargo().equals(Cargo.AnalistaNegocio.toString())) {
			System.out.println("Funcionario nao autorizado");
			return ResponseEntity.badRequest().build();
		}

		Date datahoje = new Date();
		formularioTemplate.setDataCriacao(datahoje);
		
		formularioTemplate.setFuncionalRespCriacao(funcionarioBanco.get().getFuncional());
		formularioRepo.save(formularioTemplate);

		return ResponseEntity.ok(formularioTemplate);
	}

	@RequestMapping(path="/formulario/template/cadastrar/mockado", method=RequestMethod.POST)
	public @ResponseBody FormularioTemplate cadastrarFormularioMockado() {	


		FormularioTemplate formularioTemplate = new FormularioTemplate();
		Date datahoje = new Date();

		formularioTemplate.setRamo("Idustrial");
		formularioTemplate.setSegmento("Emp1");
		formularioTemplate.setMotivo("Versao 2");
		formularioTemplate.setFuncionalRespCriacao(7235690);
		formularioTemplate.setDataCriacao(datahoje);
		formularioTemplate.setDataInicioVigencia(datahoje);

		String template = "[{\"type\":\"header\",\"subtype\":\"h1\",\"label\":\"Header\"},{\"type\":\"paragraph\",\"subtype\":\"p\",\"label\":\"Paragraph\"},{\"type\":\"autocomplete\",\"label\":\"Autocomplete\",\"className\":\"form-control\",\"name\":\"autocomplete-1524683333764\",\"values\":[{\"label\":\"Option 1\",\"value\":\"option-1\"},{\"label\":\"Option 2\",\"value\":\"option-2\"},{\"label\":\"Option 3\",\"value\":\"option-3\"}]},{\"type\":\"checkbox-group\",\"label\":\"Checkbox Group\",\"name\":\"checkbox-group-1524683334764\",\"values\":[{\"label\":\"Option 1\",\"value\":\"option-1\",\"selected\":true}]},{\"type\":\"select\",\"label\":\"Select\",\"className\":\"form-control\",\"name\":\"select-1524683337556\",\"values\":[{\"label\":\"Option 1\",\"value\":\"option-1\",\"selected\":true},{\"label\":\"Option 2\",\"value\":\"option-2\"},{\"label\":\"Option 3\",\"value\":\"option-3\"}]},{\"type\":\"text\",\"label\":\"Text Field\",\"className\":\"form-control\",\"name\":\"text-1524683340166\",\"subtype\":\"text\"},{\"type\":\"textarea\",\"label\":\"Text Area\",\"className\":\"form-control\",\"name\":\"textarea-1524683340618\",\"subtype\":\"textarea\"}]";

		formularioTemplate.setTemplate(template);

		formularioRepo.save(formularioTemplate);

		return formularioTemplate;
	}

	// retorna o templeta atual por segmento
	@RequestMapping(path="/formulario/template/segmento/{segmento}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormularioSegmento(@PathVariable String segmento) {	

		Optional<FormularioTemplate> formularioBanco;
		formularioBanco = formularioRepo.findFirstBySegmentoOrderByDataInicioVigenciaDesc(segmento);

		if(formularioBanco.isPresent()) {			
			return ResponseEntity.ok(formularioBanco);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/formulario/template/ramo/{ramo}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormularioRamo(@PathVariable String ramo) {	

		Optional<FormularioTemplate> formularioBanco;
		formularioBanco = formularioRepo.findFirstByRamoOrderByDataInicioVigenciaDesc(ramo);

		if(formularioBanco.isPresent()) {			
			return ResponseEntity.ok(formularioBanco);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/formulario/template/lista/segmento/{segmento}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getListaFormulariosSegmento(@PathVariable String segmento) {	

		Set<FormularioTemplate> setFormulariosBanco = new HashSet<FormularioTemplate>();
		setFormulariosBanco.addAll(formularioRepo.findBySegmentoOrderById(segmento));

		if(! setFormulariosBanco.isEmpty()) {			
			return ResponseEntity.ok(setFormulariosBanco);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/formulario/template/lista/ramo/{ramo}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormulariosRamo(@PathVariable String ramo) {	

		Set<FormularioTemplate> setFormulariosBanco = new HashSet<FormularioTemplate>();
		setFormulariosBanco.addAll(formularioRepo.findByRamoOrderById(ramo));

		if(! setFormulariosBanco.isEmpty()) {			
			return ResponseEntity.ok(setFormulariosBanco);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/formulario/template/lista/funcional/{funcional}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormulariosFuncional(@PathVariable Long funcional) {	

		Set<FormularioTemplate> setFormulariosBanco = new HashSet<FormularioTemplate>();
		setFormulariosBanco.addAll(formularioRepo.findByFuncionalRespCriacaoOrderById(funcional));

		if(! setFormulariosBanco.isEmpty()) {			

			return ResponseEntity.ok(setFormulariosBanco);
		}
		return ResponseEntity.badRequest().build();
	}

	// Grava um formulario cliente
	@RequestMapping(path="/formulario/cliente/cadastrar", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> preencherFormularioTeste(HttpServletRequest request, @RequestBody FormularioCadastrado formCad) {	

		Optional<Funcionario> funcionarioBanco = validarFuncionarioToken(request);

		if(! funcionarioBanco.isPresent()) {
			System.out.println("funcionaio nao encontrado ou token nao valido");
			return ResponseEntity.badRequest().build();
		}

		System.out.println(funcionarioBanco.get().getFuncional());

		if (! funcionarioBanco.get().getCargo().equals(Cargo.AnalistaNegocio.toString()) && 
				! funcionarioBanco.get().getCargo().equals(Cargo.Gerente.toString())) {
			System.out.println("nao autorizado pelo cargo");
			return ResponseEntity.badRequest().build();
		}

		Optional<Cliente> clienteBanco = clienteRepo.findByCnpj(formCad.getCnpj());

		if(! clienteBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		Date dataHoje = new Date();
		formCad.setDataCadastro(dataHoje);
		formularioCadastradoRepo.save(formCad);

		return ResponseEntity.ok(formCad);
	}

	// Grava um formulario cliente mockado
	@RequestMapping(path="/formulario/cliente/cadastrar/mockado", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> preencherFormularioMockado() {

		FormularioCadastrado formCadMockado = new FormularioCadastrado();

		Date dataHoje = new Date();
		formCadMockado.setDataCadastro(dataHoje);
		formCadMockado.setFuncionalRespCriacao(9999999);
		formCadMockado.setCnpj("9999999");
		formCadMockado.setTemplateCadastrado("[{\"type\": \"header\", \"label\": \"Header\", \"subtype\": \"h1\"}, {\"type\": \"paragraph\", \"label\": \"Paragraph\", \"subtype\": \"p\"}, {\"name\": \"autocomplete-1524683333764\", \"type\": \"autocomplete\", \"label\": \"Autocomplete\", \"values\": [{\"label\": \"Option 1\", \"value\": \"option-1\"}, {\"label\": \"Option 2\", \"value\": \"option-2\"}, {\"label\": \"Option 3\", \"value\": \"option-3\"}], \"className\": \"form-control\"}, {\"name\": \"checkbox-group-1524683334764\", \"type\": \"checkbox-group\", \"label\": \"Checkbox Group\", \"values\": [{\"label\": \"Option 1\", \"value\": \"option-1\", \"selected\": true}]}, {\"name\": \"select-1524683337556\", \"type\": \"select\", \"label\": \"Select\", \"values\": [{\"label\": \"Option 1\", \"value\": \"option-1\", \"selected\": true}, {\"label\": \"Option 2\", \"value\": \"option-2\"}, {\"label\": \"Option 3\", \"value\": \"option-3\"}], \"className\": \"form-control\"}, {\"name\": \"text-1524683340166\", \"type\": \"text\", \"label\": \"Text Field\", \"subtype\": \"text\", \"className\": \"form-control\"}, {\"name\": \"textarea-1524683340618\", \"type\": \"textarea\", \"label\": \"Text Area\", \"subtype\": \"textarea\", \"className\": \"form-control\"}]");

		formularioCadastradoRepo.save(formCadMockado);

		return ResponseEntity.ok(formCadMockado);
	}

	// seleciona formulario atual cadastrado por cnpj
	@RequestMapping(path="/formulario/cliente/consultar/cnpj/{cnpj}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormularioCadastrado(@PathVariable String cnpj) {	

		Optional<Cliente> clienteBanco = clienteRepo.findByCnpj(cnpj);

		if(! clienteBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		Optional<FormularioCadastrado> formularioCadastradoBanco;

		formularioCadastradoBanco = formularioCadastradoRepo.findFirstByCnpjOrderByDataCadastroDesc(cnpj);

		if(! formularioCadastradoBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(formularioCadastradoBanco);
	}

	// lista todos os formularios cadastrados por cnpj
	@RequestMapping(path="/formulario/cliente/lista/cnpj/{cnpj}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getVariosFormulariosCadastrados(@PathVariable String cnpj) {	

		Optional<Cliente> clienteBanco = clienteRepo.findByCnpj(cnpj);

		if(! clienteBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		Set<FormularioCadastrado> setFormulariosCadastradoBanco = new HashSet<FormularioCadastrado>();

		setFormulariosCadastradoBanco.addAll(formularioCadastradoRepo.findByCnpjOrderByDataCadastroDesc(cnpj));

		if(setFormulariosCadastradoBanco.isEmpty()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(setFormulariosCadastradoBanco);
	}

	//validar token e retornar o funcionario
	public Optional<Funcionario> validarFuncionarioToken(HttpServletRequest request) {

		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		long id = tokenService.verificar(token);

		Optional<Funcionario> funcionarioBanco = funcionarioRepo.findById(id);

		return funcionarioBanco;		
	}

}
