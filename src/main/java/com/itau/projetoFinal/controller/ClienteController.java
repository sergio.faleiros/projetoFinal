package com.itau.projetoFinal.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.projetoFinal.model.Cliente;
import com.itau.projetoFinal.model.FormularioTemplate;
import com.itau.projetoFinal.repository.ClienteRepository;
import com.itau.projetoFinal.repository.FormularioTemplateRepository;
import com.itau.projetoFinal.service.PasswordService;
import com.itau.projetoFinal.service.TokenService;


@Controller
public class ClienteController {

	@Autowired
	PasswordService passwordService;

	@Autowired
	TokenService tokenService;

	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	FormularioTemplateRepository formularioRepo;

	@RequestMapping(path="/cliente/cadastrar", method=RequestMethod.POST)
	public @ResponseBody Cliente cadastrar(@RequestBody Cliente cliente) {	

		clienteRepo.save(cliente);

		return cliente;
	}
	@RequestMapping(path="/cliente/consultar/{cnpj}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormulariosSegmento(@PathVariable String cnpj) {	

		Optional<Cliente> clienteBanco = clienteRepo.findByCnpj(cnpj);

		if(clienteBanco.isPresent()) {

			return ResponseEntity.ok(clienteBanco);
		}
		return ResponseEntity.badRequest().build();
	}
	
	@RequestMapping(path="/cliente/formulario/{cnpj}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getFormularioTemplateSegmento(@PathVariable String cnpj) {	

		Optional<Cliente> clienteBanco = clienteRepo.findByCnpj(cnpj);

		if(! clienteBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}	
		
		Date datahoje = new Date();
		Optional<FormularioTemplate> formulario;
		
		formulario = formularioRepo.findFirstBySegmentoAndDataInicioVigenciaLessThanEqualOrderByDataInicioVigenciaDesc(clienteBanco.get().getSegmento(), datahoje);

		if(formulario.isPresent()) {			

			return ResponseEntity.ok(formulario);
		}
		return ResponseEntity.badRequest().build();
	}
}
