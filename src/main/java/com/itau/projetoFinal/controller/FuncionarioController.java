package com.itau.projetoFinal.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.projetoFinal.model.Funcionario;
import com.itau.projetoFinal.repository.FuncionarioRepository;
import com.itau.projetoFinal.service.PasswordService;
import com.itau.projetoFinal.service.TokenService;


@Controller
public class FuncionarioController {

	@Autowired
	PasswordService passwordService;

	@Autowired
	TokenService tokenService;

	@Autowired
	FuncionarioRepository funcionarioRepo;

	@RequestMapping(path="/funcionario/cadastrar", method=RequestMethod.POST)
	public @ResponseBody Funcionario cadastrar(@RequestBody Funcionario funcionario) {	

		funcionario.setSenha(passwordService.encode(funcionario.getSenha()));
		funcionarioRepo.save(funcionario);

		return funcionario;
	}

	@RequestMapping(path="/funcionario/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Funcionario funcionario) {
		Optional<Funcionario> funcionarioBanco = funcionarioRepo.findByFuncional(funcionario.getFuncional());	

		if(! funcionarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(funcionario.getSenha(), funcionarioBanco.get().getSenha());

		if(deuCerto) {
			String token = tokenService.gerar(funcionarioBanco.get().getFuncional());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return new ResponseEntity<Funcionario>(funcionarioBanco.get(), headers, HttpStatus.OK);
		}

		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/funcionario/check", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> verificar(HttpServletRequest request) {

		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");

		long id = tokenService.verificar(token);

		Optional<Funcionario> funcionarioBanco = funcionarioRepo.findById(id);

		if(funcionarioBanco.isPresent()) {
			return ResponseEntity.ok(funcionarioBanco.get());
		}

		return ResponseEntity.badRequest().build();
	}
}
